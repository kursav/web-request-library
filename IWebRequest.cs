﻿using Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Utils.WebRequest
{
    public interface IWebRequestProvider
    {
        IWebRequestProvider Authorization(string userCredantials, WebRequestAuthType authtype);
        IWebRequestProvider Host(string host);
        IWebRequestProvider KeepAlive(bool keepAlive);
        Task<T> StartHttpRequestAsync<T>();
        T StartHttpRequest<T>();
        IWebRequestProvider Timeout(int timeoutAsMilis);
        IWebRequestProvider UserAgent(string userAgent);
        IWebRequestProvider Accept(string accept);
        IWebRequestProvider MediaType(string mediaTyppe);
        IWebRequestProvider ContentType(string contentType);
        IWebRequestProvider Body(object body);
        IWebRequestProvider AddHeader(string name, string value);


    }
}
