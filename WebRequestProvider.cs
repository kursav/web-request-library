﻿using Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Utils.WebRequest
{
    internal abstract class WebRequestProvider : IWebRequestProvider
    {
        public WebRequestProvider(string baseUrl, string restUrl, WebRequestHttpMethod method, SerializerType serializerType)
        {
            _serializerType = serializerType;
            switch (serializerType)
            {
                case SerializerType.Json:

                    _formatter = new JsonMediaTypeFormatter();
                    break;
                case SerializerType.Xml:

                    _formatter = new XmlMediaTypeFormatter();
                    break;
                case SerializerType.Bson:

                    _formatter = new BsonMediaTypeFormatter();
                    break;
                default:
                    break;
            }

        }
        public WebRequestProvider(string url, WebRequestHttpMethod method, SerializerType serializerType)
        {
            _serializerType = serializerType;
            switch (serializerType)
            {
                case SerializerType.Json:

                    _formatter = new JsonMediaTypeFormatter();
                    break;
                case SerializerType.Xml:

                    _formatter = new XmlMediaTypeFormatter();
                    break;
                case SerializerType.Bson:

                    _formatter = new BsonMediaTypeFormatter();
                    break;
                default:
                    break;
            }
        }



        public abstract IWebRequestProvider Authorization(string userCredantials, WebRequestAuthType authtype);
        public abstract IWebRequestProvider Host(string host);
        public abstract IWebRequestProvider KeepAlive(bool keepAlive);
        public abstract Task<T> StartHttpRequestAsync<T>();
        public abstract T StartHttpRequest<T>();
        public abstract IWebRequestProvider Timeout(int timeoutAsMilis);
        public abstract IWebRequestProvider UserAgent(string userAgent);
        public abstract IWebRequestProvider Accept(string accept);
        public abstract IWebRequestProvider MediaType(string mediaTyppe);
        public abstract IWebRequestProvider ContentType(string contentType);
        public abstract IWebRequestProvider Body(object body);
        public abstract IWebRequestProvider AddHeader(string name, string value);

        protected object _body { get; set; }
        protected MediaTypeFormatter _formatter { get; set; }
        protected SerializerType _serializerType { get; set; }
        protected string ExtractHostname(string url)
        {
            string hostname;
            //find & remove protocol (http, ftp, etc.) and get hostname

            if (url.IndexOf("//") > -1)
            {
                hostname = url.Split('/')[2];
            }
            else
            {
                hostname = url.Split('/')[0];
            }

            //find & remove port number
            hostname = hostname.Split(':')[0];
            //find & remove "?"
            hostname = hostname.Split('?')[0];

            return hostname;
        }

    }
}
