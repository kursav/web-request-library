﻿using Utils.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Utils.WebRequest
{
    internal class HttpWebRequestProvider : WebRequestProvider
    {

        HttpWebRequest request;
        public HttpWebRequestProvider(string baseUrl, string absolutePathAndQuery,
            WebRequestHttpMethod httpMethod = WebRequestHttpMethod.GET,
            SerializerType serializerType = SerializerType.Json) : base(baseUrl, absolutePathAndQuery, httpMethod, serializerType)
        {
          
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            request = (HttpWebRequest)HttpWebRequest.Create(Path.Combine(baseUrl, absolutePathAndQuery));
            request.Method = Enum.GetName(typeof(WebRequestHttpMethod), httpMethod);
            request.Host = ExtractHostname(baseUrl);
            request.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
            PopulateDefaultTypes(serializerType);
        }



        public HttpWebRequestProvider(string absoluteUrl,
        WebRequestHttpMethod httpMethod = WebRequestHttpMethod.GET,
        SerializerType serializerType = SerializerType.Json) : base(absoluteUrl, httpMethod, serializerType)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            request = (HttpWebRequest)HttpWebRequest.Create(absoluteUrl);
            request.Method = Enum.GetName(typeof(WebRequestHttpMethod), httpMethod);
            request.Host = ExtractHostname(absoluteUrl);
            request.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
            PopulateDefaultTypes(serializerType);
        }


        void PopulateDefaultTypes(SerializerType serializerType)
        {
            switch (serializerType)
            {
                case SerializerType.Json:
                    request.ContentType = "application/json";
                    request.MediaType = "application/json";
                    request.Accept = "application/json";
                    _formatter = new JsonMediaTypeFormatter();
                    break;
                case SerializerType.Xml:
                    request.ContentType = "application/xml";
                    request.MediaType = "application/xml";
                    request.Accept = "application/xml";
                    _formatter = new XmlMediaTypeFormatter();
                    break;
                case SerializerType.Bson:
                    request.ContentType = "application/bson";
                    request.MediaType = "application/bson";
                    request.Accept = "application/bson";
                    _formatter = new BsonMediaTypeFormatter();
                    break;
                default:
                    break;
            }


        }



        public override IWebRequestProvider Accept(string accept)
        {
            return this;
        }
        public override IWebRequestProvider Authorization(string userCredantials, WebRequestAuthType authtype)
        {
            request.Headers.Add("Authorization", Enum.GetName(typeof(WebRequestAuthType), authtype) + " " + userCredantials);
            return this;

        }
        public override IWebRequestProvider Body(object body)
        {
            _body = body;
            return this;

        }

        public override IWebRequestProvider AddHeader(string name, string value)
        {
            request.Headers.Add(name, value);
            return this;
        }

        public override IWebRequestProvider ContentType(string contentType)
        {
            request.ContentType = contentType;
            return this;
        }
        public override IWebRequestProvider Host(string host)
        {
            request.Host = host;
            return this;
        }
        public override IWebRequestProvider KeepAlive(bool keepAlive)
        {
            request.KeepAlive = keepAlive;
            return this;

        }
        public override IWebRequestProvider MediaType(string mediaType)
        {
            request.ContentType = mediaType;
            return this;
        }
        public async override Task<T> StartHttpRequestAsync<T>()
        {


            if (HttpWebRequest.DefaultMaximumErrorResponseLength < int.MaxValue)
                HttpWebRequest.DefaultMaximumErrorResponseLength = int.MaxValue;
            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;


            if (_body != null)
            {
                string serializedData = await _body.SerializeAsync(_serializerType).ConfigureAwait(false);
                var data = Encoding.ASCII.GetBytes(serializedData);
                var stream = await request.GetRequestStreamAsync().ConfigureAwait(false);
                if (stream == null)
                {
                    throw new Exception();
                }
                stream.Write(data, 0, data.Length);
                stream.Close();

            }
            else
            {
                request.ContentLength = 0;
            }
            //request.KeepAlive = true;
            request.KeepAlive = false;
            var _response = await request.GetResponseAsync().ConfigureAwait(false);
            HttpWebResponse response = _response as HttpWebResponse;



            if (typeof(T) == typeof(bool))
            {
                if (response.StatusCode == HttpStatusCode.OK)

                    response.Close();
                return (T)(object)true;
            }


            WebHeaderCollection headers = response.Headers;
            Stream dataStream = response.GetResponseStream();
            if (typeof(T) == typeof(string))
            {
                StreamReader reader = new StreamReader(dataStream);
                string text = await reader.ReadToEndAsync().ConfigureAwait(false);
                reader.Close();
                return (T)(object)text;
            }

            var returnType = await _formatter.ReadFromStreamAsync(typeof(T), dataStream, null, null).ConfigureAwait(false);
            dataStream.Close();
            response.Close();

            return (T)returnType;



        }
        public override T StartHttpRequest<T>()
        {

            if (HttpWebRequest.DefaultMaximumErrorResponseLength < int.MaxValue)
                HttpWebRequest.DefaultMaximumErrorResponseLength = int.MaxValue;

            if (_body != null)
            {
                string serializedData = _body.Serialize(_serializerType);
                var data = Encoding.ASCII.GetBytes(serializedData);
                using (var stream = request.GetRequestStream())
                {
                    if (stream == null)
                    {
                        return default(T);
                    }
                    stream.Write(data, 0, data.Length);

                }
            }
            else
            {
                request.ContentLength = 0;
            }
            request.KeepAlive = true;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (typeof(T) == typeof(bool))
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return (T)(object)true;
                    }
                }

                WebHeaderCollection headers = response.Headers;
                using (Stream dataStream = response.GetResponseStream())
                {
                    if (typeof(T) == typeof(string))
                    {
                        StreamReader reader = new StreamReader(dataStream);
                        string text = reader.ReadToEnd();
                        return (T)(object)text;
                    }
                    var returnType = _formatter.ReadFromStreamAsync(typeof(T), dataStream, null, null);
                    return (T)returnType.Result;
                }
            }


        }
        public override IWebRequestProvider Timeout(int timeoutAsMilis)
        {
            request.Timeout = timeoutAsMilis;
            return this;

        }
        public override IWebRequestProvider UserAgent(string userAgent)
        {
            request.UserAgent = userAgent;
            return this;

        }
    }
}
