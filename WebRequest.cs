﻿using Utils.Extensions;
using System;
using System.Net.Http.Formatting;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;
using System.Threading.Tasks;
using System.Text;
using System.IO;

namespace Utils.WebRequest
{
    public enum RequestProvider
    {
        HttpWebRequest,
        HttpClient,
        RestSharp
    }


    public enum WebRequestHttpMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }

    public enum WebRequestAuthType
    {
        Basic,
        Bearer
    }



    public sealed class WebRequest : IWebRequestProvider
    {

        static IWebRequestProvider provider;

        WebRequest()
        {

        }


        public static IWebRequestProvider Create(string baseUrl, string absolutePathAndQuery,
         RequestProvider requestProvider = RequestProvider.HttpWebRequest,
         WebRequestHttpMethod method = WebRequestHttpMethod.GET,
         SerializerType serializerType = SerializerType.Json)
        {
            switch (requestProvider)
            {
                case RequestProvider.HttpWebRequest:
                    provider = new HttpWebRequestProvider(baseUrl, absolutePathAndQuery, method, serializerType);
                    break;
                case RequestProvider.HttpClient:
                    provider = new RestSharpProvider(baseUrl, absolutePathAndQuery, method, serializerType);
                    break;
                case RequestProvider.RestSharp:
                    provider = new RestSharpProvider(baseUrl, absolutePathAndQuery, method, serializerType);
                    break;
                default:
                    break;
            }
            return provider;
        }


        public static IWebRequestProvider Create(string absoluteUrl,
         RequestProvider requestProvider = RequestProvider.HttpWebRequest,
         WebRequestHttpMethod method = WebRequestHttpMethod.GET,
         SerializerType serializerType = SerializerType.Json)
        {
            switch (requestProvider)
            {
                case RequestProvider.HttpWebRequest:
                    provider = new HttpWebRequestProvider(absoluteUrl, method, serializerType);
                    break;
                case RequestProvider.HttpClient:
                    provider = new RestSharpProvider(absoluteUrl, method, serializerType);
                    break;
                case RequestProvider.RestSharp:
                    provider = new RestSharpProvider(absoluteUrl, method, serializerType);
                    break;
                default:
                    break;
            }
            return provider;
        }


        public IWebRequestProvider Accept(string accept)
        {
            return provider.Accept(accept);
        }

        public IWebRequestProvider Authorization(string userCredantial, WebRequestAuthType authtype)
        {
            return provider.Authorization(userCredantial, authtype);
        }

        public IWebRequestProvider Body(object body)
        {
            return provider.Body(body);
        }

        public IWebRequestProvider AddHeader(string name, string value)
        {
            return provider.AddHeader(name, value);

        }

        public IWebRequestProvider ContentType(string contentType)
        {
            return provider.ContentType(contentType);
        }

        public IWebRequestProvider Host(string host)
        {
            return provider.Host(host);
        }

        public IWebRequestProvider KeepAlive(bool keepAlive)
        {
            return provider.KeepAlive(keepAlive);
        }

        public IWebRequestProvider MediaType(string mediaTyppe)
        {
            return provider.MediaType(mediaTyppe);
        }

        public T StartHttpRequest<T>()
        {
            try
            {
                return provider.StartHttpRequest<T>();
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        int statusCode = (int)response.StatusCode;
                        switch (statusCode)
                        {
                            case 400:
                                throw new BadRequestException();
                            case 401:
                                throw new UnAuthorizedException();
                            case 403:
                                throw new ForbiddenException();
                            case 404:
                                throw new NotFoundException();
                            case 409:
                                throw new ConflictException();
                            case 500:
                                throw new InternalServerErrorException();
                            case 503:
                                throw new ServiceUnavailableException();
                            default:
                                throw new Exception();
                        }
                    }
                    else
                    {
                        throw new BadRequestException();
                    }
                }
                else
                {
                    throw new BadRequestException();
                }
            }
            catch (Newtonsoft.Json.JsonSerializationException ex)
            {
                throw new JsonSerializationException();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<T> StartHttpRequestAsync<T>()
        {
            try
            {
                return provider.StartHttpRequestAsync<T>();
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        int statusCode = (int)response.StatusCode;
                        switch (statusCode)
                        {
                            case 400:
                                throw new BadRequestException();
                            case 401:
                                throw new UnAuthorizedException();
                            case 403:
                                throw new ForbiddenException();
                            case 404:
                                throw new NotFoundException();
                            case 409:
                                throw new ConflictException();
                            case 500:
                                throw new InternalServerErrorException();
                            case 503:
                                throw new ServiceUnavailableException();
                            default:
                                throw new Exception();
                        }
                    }
                    else
                    {
                        throw new BadRequestException();
                    }
                }
                else
                {
                    throw new BadRequestException();
                }
            }
            catch (Newtonsoft.Json.JsonSerializationException ex)
            {
                throw new JsonSerializationException();

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IWebRequestProvider Timeout(int timeoutAsMilis)
        {
            return provider.Timeout(timeoutAsMilis);
        }

        public IWebRequestProvider UserAgent(string userAgent)
        {
            return provider.UserAgent(userAgent);
        }

        public static string BasicUserCredantial(string username, string password)
        {
            string userCredantials = $"{username}:{password}";
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(userCredantials);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

    }


}
