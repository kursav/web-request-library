﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils.WebRequest
{
    public class NotFoundException : Exception
    {

    }

    public class BadRequestException : Exception
    {

    }
    public class UnAuthorizedException : Exception
    {

    }
    public class ForbiddenException : Exception
    {

    }
    public class ConflictException : Exception
    {

    }
    public class InternalServerErrorException : Exception
    {

    }

    public class ServiceUnavailableException : Exception
    {

    }

    public class JsonSerializationException : Exception
    {

    }
    

}
