﻿using Utils.Extensions;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Utils.WebRequest
{
    internal class RestSharpProvider : WebRequestProvider
    {
        RestClient client;
        RestRequest request;
        public RestSharpProvider(string baseUrl, string absolutePathAndQuery,
           WebRequestHttpMethod httpMethod = WebRequestHttpMethod.GET,
           SerializerType serializerType = SerializerType.Json) : base(baseUrl, absolutePathAndQuery, httpMethod, serializerType)
        {

            client = new RestClient(baseUrl);
            client.BaseHost = ExtractHostname(baseUrl);
            client.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";

            switch (serializerType)
            {
                case SerializerType.Json:
                    request = new RestRequest(absolutePathAndQuery, (Method)Enum.Parse(typeof(Method), httpMethod.ToString()), DataFormat.Json);
                    request.AddHeader("ContentType", "application/json");
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("MediaType", "application/json");
                    _formatter = new JsonMediaTypeFormatter();
                    break;
                case SerializerType.Xml:
                    request = new RestRequest(absolutePathAndQuery, (Method)Enum.Parse(typeof(Method), httpMethod.ToString()), DataFormat.Xml);
                    request.AddHeader("ContentType", "application/xml");
                    request.AddHeader("Accept", "application/xml");
                    request.AddHeader("MediaType", "application/xml");
                    _formatter = new XmlMediaTypeFormatter();
                    break;
                case SerializerType.Bson:
                    request = new RestRequest(absolutePathAndQuery, (Method)Enum.Parse(typeof(Method), httpMethod.ToString()), DataFormat.None);
                    request.AddHeader("ContentType", "application/json");
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("MediaType", "application/json"); _formatter = new BsonMediaTypeFormatter();
                    break;
                default:
                    break;
            }
        }


        public RestSharpProvider(string absoluteUrl,
       WebRequestHttpMethod httpMethod = WebRequestHttpMethod.GET,
       SerializerType serializerType = SerializerType.Json) : base(absoluteUrl, httpMethod, serializerType)
        {
            var uri = new Uri(absoluteUrl);
            var baseUrl = uri.GetLeftPart(System.UriPartial.Authority);
            var pathAndQuery = uri.PathAndQuery;

            client = new RestClient(baseUrl);
            client.BaseHost = ExtractHostname(absoluteUrl);
            client.UserAgent = @"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";

            switch (serializerType)
            {
                case SerializerType.Json:
                    request = new RestRequest(pathAndQuery, (Method)Enum.Parse(typeof(Method), httpMethod.ToString()), DataFormat.Json);
                    request.AddHeader("ContentType", "application/json");
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("MediaType", "application/json");
                    _formatter = new JsonMediaTypeFormatter();
                    break;
                case SerializerType.Xml:
                    request = new RestRequest(pathAndQuery, (Method)Enum.Parse(typeof(Method), httpMethod.ToString()), DataFormat.Xml);
                    request.AddHeader("ContentType", "application/xml");
                    request.AddHeader("Accept", "application/xml");
                    request.AddHeader("MediaType", "application/xml");
                    _formatter = new XmlMediaTypeFormatter();
                    break;
                case SerializerType.Bson:
                    request = new RestRequest(pathAndQuery, (Method)Enum.Parse(typeof(Method), httpMethod.ToString()), DataFormat.None);
                    request.AddHeader("ContentType", "application/json");
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("MediaType", "application/json"); _formatter = new BsonMediaTypeFormatter();
                    break;
                default:
                    break;
            }
        }

        public override IWebRequestProvider Accept(string accept)
        {
            request.AddHeader("Accept", accept);
            return this;
        }

        public override IWebRequestProvider Authorization(string userCredantials, WebRequestAuthType authtype)
        {
            client.AddDefaultHeader("Authorization", Enum.GetName(typeof(WebRequestAuthType), authtype) + " " + userCredantials);
            return this;
        }

        public override IWebRequestProvider Body(object body)
        {
            request.AddJsonBody(body.Serialize(SerializerType.Json));
            return this;
        }

        public override IWebRequestProvider ContentType(string contentType)
        {
            request.AddHeader("ContentType", contentType);
            return this;
        }

        public override IWebRequestProvider Host(string host)
        {
            client.BaseHost = host;
            return this;
        }

        public override IWebRequestProvider KeepAlive(bool keepAlive)
        {
            if (keepAlive)
            {
                request.AddHeader("Connection", "Keep-Alive");

            }
            return this;
        }

        public override IWebRequestProvider MediaType(string mediaTyppe)
        {
            request.AddHeader("MediaType", mediaTyppe);
            return this;
        }

        public override T StartHttpRequest<T>()
        {
            var result = client.Execute(request);
            if (typeof(T) == typeof(bool))
            {
                if (result.StatusCode == HttpStatusCode.OK)
                    return (T)(object)true;
            }
            if (typeof(T) == typeof(string))
            {
                return (T)(object)result.Content;
            }
            byte[] byteArray = Encoding.ASCII.GetBytes(result.Content);
            MemoryStream stream = new MemoryStream(byteArray);
            var formatter = new JsonMediaTypeFormatter();
            return (T)formatter.ReadFromStream(typeof(T), stream, null, null);
        }

        public async override Task<T> StartHttpRequestAsync<T>()
        {
            var result = await client.ExecuteAsync(request);
            if (typeof(T) == typeof(bool))
            {
                if (result.StatusCode == HttpStatusCode.OK)
                    return (T)(object)true;
            }
            if (typeof(T) == typeof(string))
            {
                return (T)(object)result.Content;
            }
            byte[] byteArray = Encoding.ASCII.GetBytes(result.Content);
            MemoryStream stream = new MemoryStream(byteArray);
            var formatter = new JsonMediaTypeFormatter();
            return (T)await formatter.ReadFromStreamAsync(typeof(T), stream, null, null);
        }

        public override IWebRequestProvider Timeout(int timeoutAsMilis)
        {
            client.Timeout = timeoutAsMilis; // 5000 milliseconds == 5 seconds
            return this;
        }

        public override IWebRequestProvider UserAgent(string userAgent)
        {

            client.UserAgent = userAgent;
            return this;
        }

        public override IWebRequestProvider AddHeader(string name, string value)
        {
            request.AddHeader(name, value);
            return this;
        }
    }
}
